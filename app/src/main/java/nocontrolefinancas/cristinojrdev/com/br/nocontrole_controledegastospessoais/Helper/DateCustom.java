package nocontrolefinancas.cristinojrdev.com.br.nocontrole_controledegastospessoais.Helper;

import java.text.SimpleDateFormat;

/**
 * Created by CristinoJr on 11/03/18.
 */

public class DateCustom {

    public  static  String dataAtual(){

       long data = System.currentTimeMillis();
        SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dataString = simpleDateFormat.format(data);
        return  dataString;
    }

    public static String mesAnoDataEscolhida(String data){
    //para quebar as strings por partes ex: 02/02/2020
        String retornoData[] = data.split("/");
        String dia = retornoData[0];//02
        String mes = retornoData[1];//02
        String ano = retornoData[2];//2020

        String mesAno = mes + ano;//022020
        return mesAno;


    }
}
