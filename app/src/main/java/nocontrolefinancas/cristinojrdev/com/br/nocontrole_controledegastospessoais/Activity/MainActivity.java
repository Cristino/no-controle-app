package nocontrolefinancas.cristinojrdev.com.br.nocontrole_controledegastospessoais.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;

import nocontrolefinancas.cristinojrdev.com.br.nocontrole_controledegastospessoais.Config.ConfiguracaoFirebase;
import nocontrolefinancas.cristinojrdev.com.br.nocontrole_controledegastospessoais.R;

public class MainActivity extends IntroActivity {


    private FirebaseAuth autenticacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        setButtonBackVisible(false);
        setButtonNextVisible(false);

        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.holo_blue_dark)
                .fragment(R.layout.intro_1)
                .build()


        );
        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.holo_blue_dark)
                .fragment(R.layout.intro_2)
                .build()


        );
        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.holo_blue_dark)
                .fragment(R.layout.intro_3)
                .build()


        );
        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.holo_blue_dark)
                .fragment(R.layout.intro_4)
                .build()


        );
        addSlide(new FragmentSlide.Builder()
                        .background(android.R.color.white)
                        .fragment(R.layout.intro_cadastro)
                        //para travar a tela no ultimo
                        .canGoForward(false)
                        .build()


        );

    }
    //criado o metodo onstart para após cadastrar

    @Override
    protected void onStart() {
        super.onStart();
        verificarUsuarioLogado();
    }

    public  void btnEntrar(View view){

        startActivity(new Intent(this, LoginActivity.class));

    }

    public  void btnCadastrar(View view){

        startActivity(new Intent(this, CadastroActivity.class));


    }

    public void verificarUsuarioLogado(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        //autenticacao.signOut();
        if (autenticacao.getCurrentUser() != null){
            abrirTelaPrincipal();
        }

    }

    public  void  abrirTelaPrincipal(){
        startActivity(new Intent(this, PrincipalActivity.class));
    }
}
